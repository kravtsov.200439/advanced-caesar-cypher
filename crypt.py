import string, math, hashlib #import needed modules

_chunkSize = 64 #define size of each cunk
_shiftMap = list(string.printable.replace('\r', '')) #create a map for shifting

def encrypt(msg, key): #define encryption function
    global _chunkSize, _shiftMap #include global variables
    shiftChain = [] #define a list of a chain of numbers whith which characters will be shifted on the map
    final = "" #define encrypted message/currently blank
    key = hashlib.sha256(bytes(key, 'utf-8')).hexdigest() #generate a hash of the encryption key
    msg = _toChunks(key+msg, _chunkSize) #split the message into chunks
    for i in range(len(msg)): #loop through the amount of chunks the message was split into
        shiftChain.append(_toASCII(hashlib.sha256(bytes(key+str(i), 'utf-8')).hexdigest())) #using the original key hash and a number, derive a unique shift chain for each chunk of the message
    for i in range(len(msg)): #loop through the amount of chunks the message was split into
        for j in range(len(msg[i])): #loop through each character of a chunk
            final += _shift(msg[i][j], _shiftMap, shiftChain[i][j]) #shifts the character and adds to final
    return final #returns final encrypted string

def decrypt(msg, key): #define decryption function
    global _chunkSize, _shiftMap #include global variables
    shiftChain = [] #define a list of a chain of numbers which which characters will be shifted on the map
    final = "" #define decrypted message/currently blank
    key = hashlib.sha256(bytes(key, 'utf-8')).hexdigest() #generate a hash of the encryption key
    msg = _toChunks(msg, _chunkSize) #split encrypted message into chunks
    for i in range(len(msg)): #loop through the amount of chunks the message was split into
        shiftChain.append(_toASCII(hashlib.sha256(bytes(key+str(i), 'utf-8')).hexdigest())) #using the original key hash and a number, derive a unique shift chain for each chunk of the message
    for i in range(len(msg)): #loop through the amount of chunks the message was split into
        for j in range(len(msg[i])): #loop through each character of a chunk
            final += _shift(msg[i][j], _shiftMap, -shiftChain[i][j]) #shifts the charecter back and adds to final
    if final[0:_chunkSize]==key: #if the fist chunk of message is equal to the hash of the key
        final = final.replace(final[0:_chunkSize], '') #then remove it
        return final #return final decrypted string
    else: #otherwise
        return None #return nothing

def _toChunks(string, chunkSize): #define string to list with chunks converter
    chunks = [] #declare empty chunks list
    for i in range(0, math.ceil(len(string)/chunkSize)): #loop through maximum number of chunks for the string
        try: #if isn't the last chunk
            chunks.append(string[0:chunkSize]) #add chunk to list of chunks
            string = string.replace(string[0:chunkSize], '') #remove chunk from the string
        except: #if is the last chunk
            chunks.append(string) #add chunk to list of chunks
            string='' #clear string
    for i in range(0, len(chunks)): #loop through amount of chunks
        chunks[i] = list(chunks[i]) #turn list of chunks into a list of lists of letters
    return chunks #return final list

def _toASCII(string): #define character to it's ASCII number function
    retVal = [] #make a list
    for i in string: #go through each letter
        retVal.append(ord(i)) #add ASCII number of the current letter to list
    return retVal #return list

def _shift(char, charMap, num): #define function to shift letters along a map by a given value
    index=charMap.index(char)+num #find new index value by adding shift value to the characters current index value on the map
    if index not in range(0, len(charMap)-1): #if new index goes outside of allowable indexes
        if index > len(charMap)-1: #if new index bigger than biggest allowable index
            while index > len(charMap)-1: #while new index bigger than biggest allowable index
                index-=len(charMap) #subtract the length of map from it
        elif index < 0: #if new index smaller than biggest allowable index
            while index < 0: #if new index smaller than biggest allowable index
                index+=len(charMap) #add the length of map to it
    return charMap[index] #return new shifted letter

if __name__ == "__main__": #check if this is code is being executed from the source file
    raise ValueError("This is a module. Import it to use...") #if yes, throw an error
